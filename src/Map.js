import React from 'react';
import H from "@here/maps-api-for-javascript/bin/mapsjs.bundle.harp";
import rawStyle from "./map_style.json"
import rawDarkStyle from "./map_style_dark.json"

export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
    this.map = null;
  }

  componentDidMount() {
    if (!this.map) {
      const platform = new H.service.Platform({
        apikey: "dY5Wk87MXv2ZT_fstNkosP-UampkgA8rhNe4x7TVg_s"
      });

      const layers = platform.createDefaultLayers({
        engineType: H.Map.EngineType.HARP
      });

      const engineType = H.Map.EngineType.HARP;
      const style = new H.map.render.harp.Style(rawStyle);
      // const style = new H.map.render.harp.Style(rawDarkStyle);
      const vectorLayer = platform.getOMVService().createLayer(style, { engineType });

      const map = new H.Map(
        this.ref.current,
        vectorLayer,
        {
          pixelRatio: window.devicePixelRatio,
          center: {lat: 51.5, lng: 0 },
          zoom: 6,
          engineType: H.Map.EngineType.HARP
        },
      );

      new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
      H.ui.UI.createDefault(map, layers)
      this.map = map;
    }
  }

  render() {
    return (
      <div
        style={{ width: '800px', height:'800px' }}
        ref={this.ref}
      />
    )
  }
}
